<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <header>
    	<h1 class="entry-title">
    		<?php the_title(); ?>
    	</h1>
    <?php if ( is_single() ) : ?>
    	<p class="entry-meta">
    		Posted on <time datetime="<?php echo get_the_date(); ?> <?php the_time(); ?>"><?php the_date(); ?> <?php the_time(); ?></time> 
    		by <?php the_author_link(); ?>
    	<?php 
    		// Are the comments open?
    		if ( comments_open() ) : ?>
    		&bull; <?php comments_popup_link( 'No comments', '1 comment', '% comments' ); ?>
    	<?php endif; 
    		
    		// Show categories and tags on single posts
    		if ( is_singular( 'post' ) ) :
    	?>
    		<br />Filed in <?php the_category( ', ' ); ?>
    		<?php the_tags( ' and tagged with ', ', ', '' ); ?>
    	</p>
    <?php 
    	endif;
    endif; ?>
    </header>
    <?php
    	// The content
    	the_content();
    ?>
</article>