<?php
/*
Template Name: Archives
*/

get_header(); ?>

<div id="main-container">
    <section id="content-container">
    
	<?php 
		// Start the loop
		while ( have_posts() ) : the_post(); 
	?>

		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		    <header>
		    	<h1 class="entry-title">
		    		<?php the_title(); ?>
		    	</h1>
		    </header>
		    <?php
		    	// The content
		    	the_content();
		    ?>
			<h2>Browse by month</h2>
			<ul>
			<?php 
				// Arguments
				$args = array(
					'type' => 'monthly'
				);
				
				// The archives
				wp_get_archives( $args ); 
			?>
			</ul>
			<h2>Browse by category</h2>
			<ul>
			<?php 
				// Arguments
				$args = array(
					'title_li' => ''
				);
				
				// The categories
				wp_list_categories( $args ); ?>
			</ul>
			<h2>Browse by tag</h2>
			<?php 
				// Arguments
				$args = array(
					'smallest' => 8,
					'largest' => 28,
					'number' => 0,
					'orderby' => 'name',
					'order' => 'ASC'
				);
				
				// The tag cloud
				wp_tag_cloud( $args ); ?>
		</article>

	<?php
		// Loop ends
		endwhile; 
	?>
	
    </section> <!-- #main-container ends -->
    
<?php get_sidebar(); ?>

</div>

<?php get_footer(); ?>