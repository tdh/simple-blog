README


*Simple Blog* is a WordPress theme made to demonstrate how themes are built.
You may fork it as you will, it's completely open source.

The theme was initially created for use in/with a book of mine,
_Smashing WordPress: Beyond the Blog, 4th Edition_. If you're interested you
can [read more about it here](http://tdh.me/book/smashing-wordpress-beyond-the-blog-4th-edition/).

If you enjoy and/or benefit from the *Simple Blog* theme in any way, I
appreciate a link, tweet, or even that you buy one of [my books](http://tdh.me/book/).

-- Thord Daniel Hedengren

Website: [TDH.me](http://tdh.me)
Twitter: [@tdh](http://twitter.com/tdh)