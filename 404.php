<?php get_header(); ?>

<div id="main-container">
    <section id="content-container">

        <article id="post-0" class="post no-results not-found">
            <header>
            	<h1 class="entry-title">404 Page Not Found</h2>
            </header>
            <p>Oops, this page doesn't seem to exist. Maybe the link pointing here was faulty?</p>
            <p>Would you like to search for whatever you were looking for?</p>
            <?php get_search_form(); ?>
        </article>
        
    </section> <!-- #main-container ends -->
    
<?php get_sidebar(); ?>

</div>

<?php get_footer(); ?>