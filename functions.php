<?php

// Set content width for the theme
if ( ! isset( $content_width ) )
	$content_width = 500;

// Theme setup
add_action( 'after_setup_theme', 'simpleblog_themesetup' );

function simpleblog_themesetup() {

	// Automatic feed links
	add_theme_support( 'automatic-feed-links' );

	// Add nav menus function to "init" hook
	add_action( 'init', 'simpleblog_register_menus' );

	// Add sidebars function to "widgets_init" hook
	add_action( 'widgets_init', 'simpleblog_register_sidebars' );

	// Queue JavaScript files on "wp_enqueue_scripts" hook
	add_action( 'wp_enqueue_scripts', 'simpleblog_load_scripts' );

}

// Register menus
function simpleblog_register_menus() {

	register_nav_menus(
		array(
			'top-navigation' => 'Top navigation',
			'bottom-navigation' => 'Bottom navigation'
		)
	);

}

// Register widget areas
function simpleblog_register_sidebars() {

	// Right column widget area
	register_sidebar( array(
	    'name' => 'Right column',
	    'id' => 'right-column',
	    'before_widget' => '<li id="%1$s" class="widget-container %2$s">',
	    'after_widget' => '</li>',
	    'before_title' => '<h3 class="widget-title">',
	    'after_title' => '</h3>'
	) );

}

// Queue JavaScripts
function simpleblog_load_scripts() {

	// Queue JavaScript for threaded comments if enabled
	if ( is_singular() && get_option( 'thread_comments' ) && comments_open() )
		wp_enqueue_script( 'comment-reply' );
	
}

?>