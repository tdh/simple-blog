<?php get_header(); ?>

<div id="main-container">
    <section id="content-container">
    
	<?php 
		// Start the loop
		while ( have_posts() ) : the_post(); 
		
		// Get the correct content type
		get_template_part( 'content', 'single' );

    	// Comments
    	comments_template( '', true );

		// Loop ends
		endwhile; 
	?>
	
    </section> <!-- #main-container ends -->
    
<?php get_sidebar(); ?>

</div>

<?php get_footer(); ?>